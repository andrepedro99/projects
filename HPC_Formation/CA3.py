import scipy.spatial.distance as dist
import pandas as pd
from CA1 import CA1
from Memory import Memory
from DG import DG
import numpy as np


class CA3:
    """
        Cornu Ammontis 3 Class: Responsible for the calculation of
        possible memory similarities.
    """
    def __init__(self):
        self.mem = Memory()
        self.features = []
        self.selectors = []
        self.ca1 = CA1()
        self.dg = DG()

    """
    def get_features(self):
        return self.features
    """

    def find_selector(self, e):
        """
            Function responsible for Selector Identification

            :param e: Element Selector.
            :return: Found Status and Memory Element Position of Selector
        """
        found = False
        index = -1
        i = 0
        while i < len(self.selectors):
            if np.array_equal(e, self.selectors[i]):
                found = True
                index = i
                break
            i += 1
        return found, index

    @staticmethod
    def sort_elements(features):
        """
            Static Element that orders the two least elements inside features
            list

            :param features: List of features
            :return: Values ordered and their position in memory list
        """
        min_values = []
        min_values_positions = []
        tmp = features.copy()
        num_elements = 2
        i = 0
        while i < num_elements:
            min_value = min(tmp)
            min_value_pos = tmp.index(min_value)
            min_values.append(min_value)
            min_values_positions.append(min_value_pos)
            tmp[min_value_pos] = 1000
            i += 1
        return min_values, min_values_positions

    def find_closest_element(self, features, mem_elements, packet_data):
        """
            Function that finds ambiguous elements and sends them to the
            DG section for further analysis.

            :param features: List of Measured Distances
            :param mem_elements: List of Memory Elements
            :param packet_data: Current Data
            :return: Selected Element from DG
        """
        # result = -1
        threshold = 0.25
        num_elements = len(features)
        if num_elements > 1:
            min_values, elements_positions = self.sort_elements(features)
            delta = abs(min_values[0]-min_values[1])
            if delta == 0 or delta < threshold:
                result = self.dg.untie_memory_elements(mem_elements, elements_positions, packet_data)
            else:
                result = mem_elements[elements_positions[0]]
        else:
            result = mem_elements[0]

        return result

    def feature_calc(self, packet_data):
        """
            Method for calculating the distances between the current
            data and the memory elements. Uses the Manhattan and Euclidean Distance

            :param packet_data: Current Data
            :returns: Void - Sends matched element to the CA1
        """
        grid_elements = 9
        start_pos = 3  # Not Including Top Layer
        feature_lines = []
        num_elements = self.mem.get_memory_size()
        memory = self.mem.get_memory()
        # print(self.selectors)
        # print(memory)
        # selector = packet_data[start_pos:grid_elements]
        selector = packet_data[0:start_pos]
        found, index = self.find_selector(selector)
        if found and num_elements > 0:
            mem_data = memory.get(index)
            for i in mem_data:
                man = dist.cityblock(packet_data, i)
                euc = dist.euclidean(packet_data, i)
                tmp = [man, euc]
                feature_lines.append(tmp)
            df = pd.DataFrame(feature_lines, columns=[j for j in range(len(tmp))])
            features = list(df.mean(axis=1))
            calc_result = self.find_closest_element(features, mem_data, packet_data)
            if len(mem_data) > 0:
                hl_result = self.h_learning(packet_data, mem_data)
            else:
                hl_result = calc_result

            if not np.array_equal(calc_result, hl_result):
                h1 = self.dg.hamming(packet_data, calc_result)
                h2 = self.dg.hamming(packet_data, hl_result)
                delta = h1-h2
                if delta < 0:
                    selected_element = calc_result
                else:
                    selected_element = hl_result
            else:
                selected_element = calc_result

            self.ca1.place_cell_calculation(packet_data, selected_element)

            self.mem.memory_management(found, index, packet_data)
        else:
            self.selectors.append(selector)
            self.mem.memory_management(found, index, packet_data)

    @staticmethod
    def h_learning(packet_data, mem_data):
        """
            Method responsible for validating the selecting result
            using the dot product operation

            :param packet_data: Current data
            :param mem_data: All memory elements
            :return: Matched Element
        """
        mx = np.array(mem_data)
        packet_data_t = np.transpose(np.array(packet_data))
        dot = list(np.dot(mx, packet_data_t))
        selected_value = max(dot)
        selected_value_pos = dot.index(selected_value)
        return mem_data[selected_value_pos]
