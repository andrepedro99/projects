import numpy as np
from Subiculum import Subiculum


class CA1:
    """
        Cornu Ammontis 1 Class: Responsible for identification and
        attribution of Place Cells. The identification of Place Cells
        uses the Top Layer of the Grid Cells
    """
    def __init__(self):
        self.place_cells = dict({})
        self.selectors = []
        self.s = Subiculum(self.place_cells)

    def get_place_cells_size(self):
        """
            Method that returns the Size of Place Cells

            :return: Place Cells Size
        """
        return len(self.place_cells)

    def set_place_cell(self, index, e):
        """
            Updates the Place Cell State

            :param index: Place Cell Number
            :param e: Place Cells Elements
        """
        self.place_cells.update({index: e})

    def find_selector(self, e):
        """
            Function responsible for Selector Identification

            :param e: Element Selector.
            :return: Found Status and Memory Element Position of Selector
        """
        found = False
        index = -1
        i = 0
        while i < len(self.selectors):
            if np.array_equal(e, self.selectors[i]):
                found = True
                index = i
                break
            i += 1
        return found, index

    def place_cell_calculation(self, packet_data, matched_memory_element):
        """
            Finds the Selector from the Grid Cells Top Layer and updates
            the information for Place Cell Mapping

            :param packet_data: Current data
            :param matched_memory_element: Selected Memory Element
        """
        top_layer_elements = 3
        start_pos = 3
        grid_elements = 9
        selector_packet_data = packet_data[start_pos:grid_elements]
        selector_matched_memory = packet_data[start_pos:grid_elements]
        print(self.selectors)
        # selector_packet_data = packet_data[0:top_layer_elements]
        # selector_matched_memory = matched_memory_element[0:top_layer_elements]
        if np.array_equal(selector_packet_data, selector_matched_memory):
            current_selectors = [selector_packet_data]
            current_elements = [packet_data]
        else:
            current_selectors = [selector_packet_data, selector_matched_memory]
            current_elements = [packet_data, matched_memory_element]

        self.place_cell_management(current_selectors, current_elements)
        print(self.place_cells)
        print("\n")
        self.s.pc_mapping()

    def place_cell_management(self, c_selectors, c_elements):
        """
            Method responsible for addition and identification of Place Cells Elements

            :param c_selectors: Current Selectors
            :param c_elements: Current Elements
        """
        num_elements = self.get_place_cells_size()
        for i in range(len(c_selectors)):
            found, index = self.find_selector(c_selectors[i])
            if found and num_elements > 0:
                pc_data = self.place_cells.get(index)
                if c_elements[i] not in pc_data:
                    pc_data.append(c_elements[i])
                self.place_cells.update({index: pc_data})
            else:
                self.place_cells.update({num_elements: [c_elements[i]]})
                self.selectors.append(c_selectors[i])


