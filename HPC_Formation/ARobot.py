import numpy as np
import math
import matplotlib.pyplot as plt
from random import *
import threading

from EC import EC 
from CA3 import CA3
from CA1 import CA1
from Subiculum import Subiculum

from Vision import Vision

from controller import Supervisor
from controller import DistanceSensor
from controller import Keyboard, Camera
from controller import Accelerometer, Gyro
from controller import Compass

import pickle


robot = Supervisor()

keyboard = Keyboard()
camera = Camera("camera")
gyro = Gyro("gyro")
accelerometer = Accelerometer("accelerometer")
left_sensor = DistanceSensor("left infrared sensor")
front_sensor = DistanceSensor("front infrared sensor")
front_left_sensor = DistanceSensor("front left infrared sensor")
front_right_sensor = DistanceSensor("front right infrared sensor")
right_sensor = DistanceSensor("right infrared sensor")
compass_xy = Compass("compassXY_01")
compass_z = Compass("compassZ_01")


TIME_STEP = int(robot.getBasicTimeStep())
MAX_SPEED = 47.61
MOVING_SPEED = 6
ROTATE_SPEED = 6
ENV_MAP = []

left_motor = robot.getMotor('left wheel motor')
right_motor = robot.getMotor('right wheel motor')


def cal_bearing(compass_x,compass_z):
    rad = math.atan2(compass_x,compass_z)
    bearing = (rad-(math.pi/2))/math.pi*180
    if bearing < 0:
        bearing += 360
    return bearing

def recon_env(rotation_time, actual_time):
    stop = False
    if actual_time <= rotation_time:
        left_mov()
    else:
        stop = True
    return stop

def random_movements():
    stop()
    num_mov = [i for i in range(0,3)]
    selected_move = choice(num_mov)
  
    if selected_move == 0:
        left_mov()
    elif selected_move == 1:
        accelerate()
    elif selected_move == 2:
        right_mov()
    else:
        raise Exception("Unsupported Operation")
       
    return selected_move

def accelerate():
    left_motor.setVelocity(MOVING_SPEED)
    right_motor.setVelocity(MOVING_SPEED)
    
def right_mov():
    left_motor.setVelocity(ROTATE_SPEED)
    right_motor.setVelocity(-ROTATE_SPEED)

def left_mov():
    left_motor.setVelocity(-ROTATE_SPEED)
    right_motor.setVelocity(ROTATE_SPEED)
    
def stop():
    left_motor.setVelocity(0.0)
    right_motor.setVelocity(0.0)
    

def draw_one(stripe):
    # print("Inside Draw Stripes")
    possible_colors = ['r', 'g', 'b', 'y']
    possible_symbols = ['*', '+', '.', 'o']
    color_list = []
    sym_list = []
    color_results = []
    sym_results = []

    actual = stripe
    tmp_color = [0]*3
    tmp_sym = [0]*3

    for j in range(len(actual)):
        if np.array_equal(actual[j], [2, 1, 2]) or np.array_equal(actual[j], [1, 3, 1]) or \
               np.array_equal(actual[j], [3, 2, 3]):
            tmp_color[j] = possible_colors[0]
            tmp_sym[j] = possible_symbols[0]
        elif np.array_equal(actual[j], [1, 2, 2]) or np.array_equal(actual[j], [2, 3, 3]) or \
                    np.array_equal(actual[j], [3, 1, 1]):
            tmp_color[j] = possible_colors[1]
            tmp_sym[j] = possible_symbols[1]
        elif np.array_equal(actual[j], [1, 1, 3]) or np.array_equal(actual[j], [3, 3, 2]) or \
                    np.array_equal(actual[j], [2, 2, 1]):
            tmp_color[j] = possible_colors[2]
            tmp_sym[j] = possible_symbols[2]
        else:
            tmp_color[j] = possible_colors[3]
            tmp_sym[j] = possible_symbols[3]
        
    return tmp_color, tmp_sym


def plot_t(x, y, color, sym):
    filename = "result_t.png"
    for j in range(len(color)):
        if j == 2:
            plt.plot(x, y, c=color[j], marker=sym[j], ms=1)
        else:
            continue
    plt.savefig(filename)
    
    
def write_to_file(all):
    filename = "d.data"
    with open(filename,'wb') as out:
        pickle.dump(all,out)
        

def write_to_file_pc(all):
    filename = "pc.data"
    with open(filename,'wb') as out:
        pickle.dump(all,out)



def plotting(ec, all):
    print("Inside Plotting")
    ec.plot_map03(all)


def main():
    
    direction = 0
    sim_time = 0
    stop_time = 20000
    
    sample_period = 10
    video_sample_period = 1
    angle_sample_period = 10
    
    img_means = []
    camera.enable(video_sample_period)
    
    keyboard.enable(sample_period)
    gyro.enable(angle_sample_period)
    # accelerometer.enable(sample_period)
    
    sensor_array = [0]*5
    left_sensor.enable(sample_period)
    front_sensor.enable(sample_period)
    front_left_sensor.enable(sample_period)
    front_right_sensor.enable(sample_period)
    right_sensor.enable(sample_period)
    
    
    x_map = []
    y_map = []
    stripes = []
    
    x = 0
    y = 0
    
    env_x = 0
    env_y = 0
    
    div_value = 1
    
    axis_num = 3
    angles = []
   
   
    angles = [[(math.pi/3),(math.pi/3),(5*math.pi/3)]]*axis_num
    actual_angles = angles
    stripe = [[0]*3]*axis_num
    count = 0
    seed(256)
    
    ac_robot = robot.getFromDef("ROBOT1")
    mov_in_field = ac_robot.getField('translation')
    compass_sample_period = 1
    
    compass_xy.enable(compass_sample_period)
    compass_z.enable(compass_sample_period)
    
    rdir = []
    estdir = []
    data = []
    all = []
    
    ec = EC()
    ca3 = CA3()
    ca1 = CA1()
    sub = Subiculum()
    
    random_firing_number = 26
    pc_firing_flag = 1
    
    # t = threading.Thread(target = sub_obj.rl_q_learning)
    t = None
    q_table = None
    current_pcs = None
    
    hd_error = 0.07
    while robot.step(TIME_STEP) != -1:
        print("Sim Time: "+str(sim_time))
        
        ca1_obj = ca3.get_ca1_obj()
        initial_size = ca1_obj.get_place_cells_size()
        
        
        # th = threading.Thread(target=lambda q, cp, pc_el: q.put(sub.pc_mapping(cp,pc_el)), args=(que, current_pcs, pc_elements))
        obstacle = False
        pc_firing_flag = None
        v = Vision(camera)
        left_motor.setPosition(float('inf'))
        right_motor.setPosition(float('inf'))
        
        sensor_array[0] = left_sensor.getValue()
        sensor_array[1] = front_sensor.getValue()
        sensor_array[2] = front_left_sensor.getValue()
        sensor_array[3] = front_right_sensor.getValue()
        sensor_array[4] = right_sensor.getValue()
        
        print(sensor_array)
        
        g = np.around(gyro.getValues(),2)
        # a = np.around(accelerometer.getValues(),2)
        
        left_motor_velocity = left_motor.getVelocity()
        right_motor_velocity = right_motor.getVelocity()
       
        robot_pos = mov_in_field.getSFVec3f()       
       
        
        c_xy = np.around(compass_xy.getValues(),2)
        c_z = np.around(compass_z.getValues(),2)
        
        real_dir = (cal_bearing(c_xy[0],c_z[2]))*math.pi/180
        # real_dir = (cal_bearing(c_xy[0],c_z[2]))
            
        rdir.append(real_dir)
        hd_err = real_dir*(1-(uniform(0,1)*hd_error-hd_error/2))
        
        """    
        if left_motor_velocity == -ROTATE_SPEED:
            direction = (direction-1) % len(img_means)
        elif right_motor_velocity == -ROTATE_SPEED:
            direction = (direction+1) % len(img_means)
        """ 
        # accelerate()
            
        if count == 0:
            # select_move = random_movements()
            accelerate()
            if sim_time % random_firing_number == 0:
                pc_firing_flag = randint(0,1)
            # pc_firing_flag = randint(0,1)
        else:
            left_mov()
            count -= 1
        
        # print("PCFire: "+str(pc_firing_flag))
        
        if left_motor_velocity == MOVING_SPEED and right_motor_velocity == MOVING_SPEED:
            x = (x + MOVING_SPEED * math.cos(real_dir))/div_value
            y = (y + MOVING_SPEED * math.sin(real_dir))/div_value
            
            # env_x = robot_pos[0] 
            # env_y = robot_pos[2]
                
            # new_angles, new_stripe = ML.find_position(real_dir, (left_motor_velocity/450), actual_angles)
            # Check for Real Direction
            print("HD Error: "+str(abs(real_dir-hd_err)))
            new_angles, new_stripe = ec.find_position(real_dir, (left_motor_velocity/660), actual_angles)
            actual_angles = new_angles
            stripe = new_stripe
            
            
        env_x = robot_pos[0] 
        env_y = robot_pos[2] 
            
            # print("X: "+str(env_x))
            # print("Y: "+str(env_y))
            
        obs_value = max(sensor_array)
        if obs_value > 400:
             # left_mov()
            obstacle = True
            count = randint(10,30)
            
            """
            current_key = keyboard.getKey()
            if current_key == Keyboard.LEFT:
                print("Key Pressed")
                stop()
            """           
            """
            current_key = keyboard.getKey()
            if current_key == Keyboard.LEFT:
                left_mov()
            elif current_key == Keyboard.RIGHT:
                right_mov()
            elif current_key == Keyboard.UP:
                accelerate()
            """
            
        x_map.append(env_x)
        y_map.append(env_y)
        stripes.append(stripe)
        
        
        tmp = [stripe, real_dir]+sensor_array+[v.quadrant_means()]
        enc_data = ec.data_encoding(tmp)
        cs = ec.draw_stripes_one(stripe)
        tmp_pos = [env_x, env_y] + cs
        
        all.append(tmp_pos)
       
        # print(obstacle)
        ca3.feature_calc(enc_data,env_x,env_y,data,obstacle, t)
        
        current_pcs = ca1_obj.get_place_cells()
        pc_elements = list(current_pcs.keys())
        # print(current_pcs)
        
        current_size = ca1_obj.get_place_cells_size()
        th = threading.Thread(target = sub.pc_mapping, args=(current_pcs, pc_elements,))
        
        if current_size > 1 and initial_size != current_size:
            th.start()
            th.join()
            # del th
            # print(sub.get_qtable())
            # sub.pc_mapping(current_pcs, pc_elements)
            # sub.pc_mapping(current_pcs, pc_elements)
            q_table = sub.get_qtable()
            
            
            # sub.pc_mapping(current_pcs, pc_elements)
        
        
        if sim_time == stop_time:
            # th.join()
            # del th
            print("Stopping")
            stop()
            break
        
        sim_time += 1
   
   
    write_to_file(all)
    write_to_file_pc(data)
    # print(current_pcs)
    # print(q_table)
    sub.plot_network_graph(q_table, current_pcs)
    print("\n")
    print("Finished")
    print("\n")
    
    # ca1.plot_pc(data)
    
    # ec.plot_map03(all)
    # c_list, sym_list = ec.draw_stripes(stripes)
    # ec.plot_map02(x_map, y_map,c_list,sym_list)
    # print(c_list)
    # print(sym_list)
    # ML.draw_stripes(x_map, y_map, stripes)
    # print("Plotting") 
    # write_to_file(data)
    # ML.plot_directions(rdir, estdir)
    # ec.test()
    # ec.draw_stripes(x_map, y_map, stripes)
    return 0
    
if __name__ == "__main__":
    main()
    
