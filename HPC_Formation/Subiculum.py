import numpy as np
import scipy.spatial.distance as dist
from random import *
import networkx as nx
import matplotlib.pyplot as plt


class Subiculum:
    def __init__(self, place_cells):
        self.place_cells = place_cells
        self.previous_pc_size = 0

    def pc_mapping(self):
        """
            Creates a map of the different connections between Place
            Cells. It uses the Q-learning to find those connections.

            After the map calculating that is plotted using a networkx, a
            library used for graph plotting.

        """
        all_place_cells = list(self.place_cells.keys())
        pc_length = len(self.place_cells)
        if pc_length > 1:
            reward_table = self.reward_table_dists(all_place_cells)
            print("R: "+str(reward_table))
            q_table = np.zeros((pc_length, pc_length))
            learning_param = 0.8  # Gamma Value
            num_it = 12

            for _ in range(num_it):
                chosen_state = choice(all_place_cells)
                possible_actions = reward_table[chosen_state]
                available_actions = [i for i in range(len(possible_actions)) if possible_actions[i] != 0]
                chosen_action = choice(available_actions)
                q_next_values = q_table[chosen_action]
                current_reward = reward_table[chosen_state][chosen_action]
                q_value = current_reward + learning_param * max(q_next_values)

                q_table[chosen_state][chosen_action] = q_value

            if pc_length != self.previous_pc_size:
                self.plot_network_graph(q_table)
                self.previous_pc_size = pc_length

    def reward_table_dists(self, all_place_cells):
        """
            Calculates the rewarding table necessary for the
            Q-learning calculation

            :param all_place_cells: Place Cells Information
            :return: Rewarding table
        """
        reward_table = []
        num_elements = len(all_place_cells)
        if num_elements > 1:
            i = 0
            while i < num_elements:
                pc_dists = []
                current_pc = self.place_cells.get(i)
                for j in all_place_cells:
                    other_pc = self.place_cells.get(j)
                    d = self.calc_dists(current_pc, other_pc)
                    pc_dists.append(d)
                reward_table.append(pc_dists)
                i += 1
            return reward_table

    @staticmethod
    def calc_dists(current_pc, other_pc):
        """
            Calculates the Distances between different Place
            Cells elements

            :param current_pc: First Place Cell
            :param other_pc: Second Place Cell
            :return: Distance Average of all elements
        """
        distance = dist.cdist(current_pc, other_pc)
        return distance.mean()

    def plot_network_graph(self, q_table):
        """
            Plots the connections using the networkx

            :param q_table: Connections obtained from the Q-learning
            :return: Figure with the map
        """
        plt.clf()
        g = nx.Graph()
        edg = []
        mx_size = q_table.shape
        mx_size_value = mx_size[0]  # Squared Matrix
        i = 0
        while i < mx_size_value:
            for j in range(mx_size_value):
                if q_table[i][j] != 0:
                    tmp = [i, j]
                    edg.append(tmp)
            i += 1

        g.add_nodes_from(self.place_cells)
        g.add_edges_from(edg)
        nx.draw(g, with_labels=True)
        plt.savefig("graph_map.png")



