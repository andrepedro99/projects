import scipy.spatial.distance as dist
import pandas as pd
import numpy as np


class DG:
    """
        Dentate Gyrus Class: Separates ambiguous elements using the Cosine Similarity
        formula and the Hamming Distance
    """

    def __init__(self):
        pass

    def untie_memory_elements(self, mem_elements, elements_positions, packet_data):
        """

            Method that further separates elements using the Cosine Similarity Formula
            and the Hamming Distance

            Cosine Similarity Formula
                :math:`cos(x) = {A . B} / {|| A || . || B ||}`

            :param mem_elements: List of Memory Elements
            :param elements_positions: Positions of the ambiguous elements
            :param packet_data: Current data
            :return: Selected Memory Element
        """
        feature_lines = []
        for i in elements_positions:
            current = mem_elements[i]
            h_dist = self.hamming(packet_data, current)
            cos_dist = dist.cosine(packet_data, current)
            tmp = [h_dist, cos_dist]
            feature_lines.append(tmp)
        df = pd.DataFrame(feature_lines, columns=[j for j in range(len(tmp))])
        features = list(df.mean(axis=1))
        selected_value = min(features)
        selected_value_position = features.index(selected_value)  # Find Min Index
        mem_position = elements_positions[selected_value_position]  # Find Mem Position Index

        return mem_elements[mem_position]

    @staticmethod
    def hamming(a, b):
        """
            Method that calculates the Hamming Distance

            :param a: First Element
            :param b: Second Element
            :return: Hamming Distance Result
		"""
        result = 0
        for i in range(len(a)):
            if a[i] != b[i]:
                result += 1
        return result
