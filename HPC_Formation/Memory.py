import numpy as np
import matplotlib.pyplot as plt


class Memory:
    def __init__(self):
        self.memory = dict({})
        self.all = []
        self.reward = []
        self.num_curve_elements = 1000
        self.curve_elements = self.forgetting_curve()
        self.reward_max_value = max(self.curve_elements)

    def get_memory(self):
        return self.memory

    def set_memory(self, index, mem_data):
        self.memory.update({index: mem_data})

    def get_memory_size(self):
        return len(self.memory)

    def get_reward_size(self):
        return len(self.reward)

    def memory_management(self, found, index, packet_data):
        """
            Method responsible to add new memory elements and their
            respective rewards

            :param found: Selector Identification Flag
            :param index: Selector Position
            :param packet_data: Current Data
        """
        memory_size = self.get_memory_size()
        if found and memory_size > 0:
            mem_data = self.memory.get(index)
            if packet_data not in mem_data:
                mem_data.append(packet_data)
            self.set_memory(index, mem_data)
            self.all.append(packet_data)
            self.reward.append(self.reward_max_value)
        else:
            mem_data = [packet_data]
            self.set_memory(memory_size, mem_data)
            self.all.append(packet_data)
            self.reward.append(self.reward_max_value)

        delete_elements = self.reward_management(packet_data)
        if len(delete_elements) > 0:
            self.delete_event(delete_elements)

    def reward_management(self, packet_data):
        """
            Manages the reward system to simulate the brain events
            forgetting behaviour.

            :param packet_data: Current Data
            :return: Elements to be deleted
        """
        delete_elements = []
        for i in range(len(self.all)):
            if not np.array_equal(packet_data, self.all[i]):
                value = self.reward[i]
                value_pos = self.curve_elements.index(value)
                if value_pos == len(self.curve_elements)-1:
                    delete_elements.append(self.all[i])
                else:
                    self.reward[i] = self.curve_elements[value_pos+1]
        return delete_elements

    def delete_event(self, delete_elements):
        """
            Deletes the previous marked elements

            :param delete_elements: Elements to be deleted
        """
        num_elements = self.get_memory_size()
        i = 0
        while i < len(delete_elements):
            current = delete_elements[i]
            for j in range(num_elements):
                mem_data = self.memory.get(j)
                if current in mem_data:
                    mem_data.remove(current)
                    break
            event_pos = self.all.index(current)
            self.all.pop(event_pos)
            self.reward.pop(event_pos)
            i += 1

    def forgetting_curve(self):
        """
            Generates the brains forgetting curve according to
            the Ebbinghaus formula

            Ebbinghaus Formula
                :math:`{1.84} / {log_{10}(x)^{1.25} + 1.84}`

            :return: Formula Elements
        """
        zero_value = 500
        inc_value = zero_value / self.num_curve_elements
        start = 1
        stop = zero_value
        # curve_elements = [(-np.log(i)+2)/2 for i in np.arange(start, stop, inc_value)]
        curve_elements = [(1.84/(np.power(np.log10(i), 1.25)+1.84)) for i in np.arange(start, stop, inc_value)]
        return curve_elements

