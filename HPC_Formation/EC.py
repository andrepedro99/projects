import math
import numpy as np


class EC:

    def __init__(self):
        self.axis_num = 3
        self.angles_direction = [(-30)/180*math.pi, 90/180*math.pi, 210/180*math.pi]

    def find_position(self, direction, mov_speed, actual_angles):
        """
            Method responsible for receiving the elements current
            status, calculates the current angles and returns the
            Stripes necessary to create the Grid Cells

            :param direction: Current Direction
            :param mov_speed: Current Speed
            :param actual_angles: Current Angles
            :return: Stripes for the Grid Cells Formation and the New Angles
        """
        hd_axis = np.array([direction]*self.axis_num)
        circle_cos = np.cos(np.subtract(hd_axis, np.array(self.angles_direction)))
        new_angles = self.cal_angle(actual_angles, mov_speed, circle_cos)
        actual_angles = new_angles
        new_stripe = self.cal_stripe(actual_angles)
        stripe = new_stripe
        return actual_angles, stripe

    def cal_angle(self, angles, mov_speed, circle_cos):
        """
            Calculates the new angles after the circle layers
            rotation

            :param angles: Current Rotation
            :param mov_speed: Current Speed
            :param circle_cos: Older Angles
            :return: New Angles for Stripe
        """

        result = [0] * self.axis_num
        div_count = len(angles) - 1

        i = 0
        while i < len(angles):
            actual = angles[i]
            tmp = [0] * self.axis_num
            div_value = math.pow(2, div_count)
            for j in range(len(actual)):
                tmp[j] = (actual[j] + mov_speed / div_value * circle_cos[j]) % (2 * math.pi)
            result[i] = tmp
            div_count -= 1
            i += 1
        return result

    def cal_stripe(self, angles):
        """
            Method responsible for the Stripes calculation. The
            junction of different stripes creates the Grid Cells

            :param angles: Current Angles obtained from cal_angles()
            :return: Stripes Configuration
        """
        result = [0]*self.axis_num
        i = 0
        while i < len(angles):
            actual = angles[i]
            tmp = [0]*self.axis_num
            if i == 1:
                for j in range(len(actual)):
                    if j == 2:
                        if actual[j] <= 2*math.pi/3:
                            tmp[j] = 2
                        elif actual[j] > 4*math.pi/3:
                            tmp[j] = 3
                        else:
                            tmp[j] = 1
                    else:
                        if actual[j] <= 2*math.pi/3:
                            tmp[j] = 1
                        elif actual[j] > 4*math.pi/3:
                            tmp[j] = 2
                        else:
                            tmp[j] = 3
            else:
                for j in range(len(actual)):
                    if actual[j] <= 2*math.pi/3:
                        tmp[j] = 1
                    elif actual[j] > 4*math.pi/3:
                        tmp[j] = 3
                    else:
                        tmp[j] = 2

            result[i] = tmp
            i += 1
        return result

