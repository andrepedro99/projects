import unittest
import numpy as np
from EC import EC
from DG import DG
from CA3 import CA3

ec = EC()
dg = DG()
ca3 = CA3()


class MyTestCase(unittest.TestCase):
    """
        Tests for some of the implemented methods
    """
    def test_encoding_non_cyclic_data(self):
        """
            Checks the encoding method on a non-cyclic value.

            :return: Must be equal to [1, 3, 2, 1, 1, 2]
        """
        number = 23
        cyclic = False
        obtained_result = ec.encoding_method(number, cyclic)
        expected_result = [1, 3, 2, 1, 1, 2]
        self.assertEqual(obtained_result, expected_result, "Encoding must be the same")

    def test_encoding_cyclic_data(self):
        """
             Checks the encoding method on a cyclic value.

            :return: Must be equal to [3, 3, 2, 1, 1, 2]
        """
        number = 23
        cyclic = True
        obtained_result = ec.encoding_method(number, cyclic)
        expected_result = [3, 3, 2, 1, 1, 2]
        self.assertEqual(obtained_result, expected_result, "Encoding must be the same")

    def test_extract_grid_data(self):
        """
            Tests the extraction of layer information
        """
        grid = [[[1, 1, 3], [1, 1, 3], [1, 1, 3]], 2]
        obtained_result = ec.extract_gc(grid)
        expected_result = [1, 1, 3]+[1, 1, 3]+[1, 1, 3]
        self.assertEqual(obtained_result, expected_result, "Elements must be together")

    def test_hamming(self):
        """
            Tests the Hamming Distance Function

            :return: Values between paper and method must be the same
        """
        a = [1, 1, 2, 1, 1, 2]
        b = [1, 0, 2, 1, 1, 1]
        obtained_result = dg.hamming(a, b)
        expected_result = 2
        self.assertEqual(obtained_result, expected_result, "Results should be the same")

    def test_sort_elements(self):
        """
            Tests the sorting method used in CA3
        """
        features = [10, 1, 7, 6, 5, 15]
        ob_min_values, ob_min_values_pos = ca3.sort_elements(features)
        exp_min_values = [1, 5]
        exp_min_values_pos = [1, 4]
        self.assertEqual(ob_min_values, exp_min_values)
        self.assertEqual(ob_min_values_pos, exp_min_values_pos)

    def test_h_learning(self):
        """
            Tests the System Validator Mechanism
        """
        memory_elements = np.array([[1, 1, 2, 2, 1, 3], [2, 1, 1, 1, 1, 3], [1, 2, 1, 2, 1, 2],
                                    [3, 1, 1, 2, 1, 3], [1, 2, 3, 2, 2, 3], [3, 1, 1, 2, 1, 3]])
        current_data = np.transpose(np.array([1, 1, 1, 2, 1, 3]))
        obtained_result = list(ca3.h_learning(current_data, memory_elements))
        expected_result = [1, 2, 3, 2, 2, 3]
        self.assertEqual(obtained_result, expected_result)


if __name__ == '__main__':
    unittest.main()
