#!/bin/bash

printf "\n";
echo "Virtual Hippocampus Config File";
printf "\n";

echo "Updating System Configurations...";
printf "\n";
sudo apt-get update
sudo apt-get dist-upgrade

printf "\n";
echo "Installing Python Dependencies";

printf "\n";
echo "Installing Numpy...";
sudo apt-get install python3-numpy

printf "\n";
echo "Installing Scipy...";
sudo apt-get install python3-scipy

printf "\n";
echo "Installing Pandas...";
sudo apt-get install python3-pandas

printf "\n";
echo "Dependencies Installation Complete";
printf "\n";
echo "Execution Instructions";
echo " - Change to the VHPC Directory";
echo " - Execute python3 Main.py";
printf "\n";
