import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt

class Vision():
	"""
		Class to Process Image Frames
	"""
    def __init__(self, camera):
        self.__camera = camera
        self.__img = camera.getImage()
        self.__name_img = "teste_.jpg"
        self.__save = camera.saveImage(self.__name_img,100)
        self.__frame = cv.imread(self.__name_img,cv.IMREAD_GRAYSCALE)
        self.__clean = self.sobel_operator()
        self.__shape = self.__clean.shape
   
    def pix_mean(self):
        return np.average(self.__frame)
    
    def sobel_operator(self):
        sobel_x = cv.Sobel(self.__frame, cv.CV_64F,1,0,ksize = 3)
        sobel_y = cv.Sobel(self.__frame, cv.CV_64F,0,1,ksize = 3)
        result = cv.addWeighted(sobel_x,0.5,sobel_y,0.5,0)
        return result
    
    def quadrant_means(self):
        height = self.__shape[0]
        width = self.__shape[1]
        mask_w = width // 2
        mask_h = height // 2
        result = []
        possible_values_w = [i for i in range(0,width+1,mask_w)]
        possible_values_h = [i for i in range(0,height+1,mask_h)]
        i = 0
        while i < len(possible_values_w):
            if i+1 < len(possible_values_w):
                start_w = possible_values_w[i]
                stop_w = possible_values_w[i+1]
                for j in range(len(possible_values_h)):
                    if j+1 < len(possible_values_h):
                        start_h = possible_values_h[j]
                        stop_h = possible_values_h[j+1]
                        window = self.__clean[start_w:stop_w,start_h:stop_h]
                        result.append(np.mean(window))
            i += 1
        img_mean = sum(result)/len(result)
        return img_mean                              
        
        
        