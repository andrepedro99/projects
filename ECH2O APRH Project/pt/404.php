<html>
	<head>
		<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">	
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
		<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
	</head>
	<body>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="error-template" style="margin-top:20%;">
					<h1>
						Calculadora da Pegada Hídrica</h1>
					<h2></h2>
					<div class="error-details">
						Por razões de ordem técnica a Calculadora da Pegada Hídrica encontra-se temporariamente
						indisponível. Lamentamos o transtorno causado!
					</div>
					<!--<div class="error-actions">
						<a href="http://www.jquery2dotnet.com" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-home"></span>
						Take Me Home </a><a href="http://www.jquery2dotnet.com" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-envelope"></span> Contact Support </a>
					</div>-->
				</div>
			</div>
		</div>
	</div>
	</body>
</html>