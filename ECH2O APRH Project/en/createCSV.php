<?php

	include 'connect_db.php';
	
	/*
		Function that returns all users from DB
		return: List of users with ID and Timestamp of creation
	*/
	function get_all_users(){
	
		$conn=connectDB();
		
		$sql="SELECT * FROM utilizadores";
		$result=$conn->query($sql);
		
		$users=array();
		
		if($result->num_rows>0){
			while($row=$result->fetch_assoc()){
				
				$id_user=$row["id"];
				
				$sql02="SELECT * FROM dados WHERE id_user=$id_user";
				$result02=$conn->query($sql02);
				
				if($result02->num_rows>0){
					$timestamp=$row["time"];
					$users[$id_user]=$timestamp;
				}
			}
		}
		
		return $users;
		
	}
	
	/*
		Function that returns the answers from a specific user
		return: List of answers 
	*/
	function get_user_data($id_user){
		
		$conn=connectDB();
		$sql="SELECT * FROM dados WHERE id_user=$id_user";
		$result=$conn->query($sql);
		
		$user_data=array();
		$count = 0;
		
		if($result->num_rows>0)
		{
			while($row=$result->fetch_assoc()){
				$user_data[$count]=$row["resultado"];
				$count = $count+1;
			}
			
		}
		else
			$user_data[0] = "No Data Avaliable";
		
		return $user_data;
		
	}
	
	/*
		Function that returns the water footprint results from a 
		specific user
		return: List of results
	*/
	function get_user_fresults($id_user){
		
		$conn=connectDB();
		
		$sql="SELECT resultado_rea, resultado_vir FROM dadosfinais WHERE id_user='$id_user';";
		$result=$conn->query($sql);
		$final_results=array();
		if($result->num_rows>0){
			while($row=$result->fetch_assoc()){
				$final_results[0]=$row["resultado_rea"];
				$final_results[1]=$row["resultado_vir"];
			}
		}
		else{
			$final_results[0] = "No Data Avaliable";
			$final_results[1] = "No Data Avaliable";
		}
		
		return $final_results;
		
	}
	
	
	/*
		Function that puts together the previous information
		return: Data from DB
	*/
	function data_matrix(){
		$conn = connectDB();
		
		$users=get_all_users();
		$answers=array();
		$resultS=array();
		$user_info=array();
		$file_data=array();
		
		$count = 0;
		foreach($users as $id_user => $timestamp){
			$user_info[0]=$id_user;
			$user_info[1]=$timestamp;
			$answers=get_user_data($id_user);
			$results=get_user_fresults($id_user);
			$tmp=array_merge($user_info,$answers,$results);
			$file_data[$count]=$tmp;
			$count = $count + 1;
		}
		
		return $file_data;
	}
	
	
	/*function get_users_data(){
		
		$conn=connectDB();
		
		$users=get_all_users();
		$user_data=array();
		$file_data=array();
		$line_number=1;
		foreach($users as $id_user=>$timestamp){
			$final_results=get_user_fresults($id_user);
			$count=0;
			//$sql="SELECT * FROM dados WHERE id_user=$id_user";
			//$result=$conn->query($sql);
			$user_data[$count]=$id_user;
			$count=$count+1;
			
			/*if($result->num_rows>0)
			{
				while($row=$result->fetch_assoc()){
						$user_data[$count]=$row["resultado"];
						$count=$count+1;
				}
			}
			$user_data=get_user_data($id_user,$count);
			$count=count($user_data)-1;
			
			$user_data[$count]=$timestamp;
			$count=$count+1;
			
			if($final_results){
				$user_data[$count]=$final_results[0];
				$count=$count+1;
				$user_data[$count]=$final_results[1];
			}	
			else{
				$user_data[$count]="No Data Avaliable";
				$count=$count+1;
			}
			
			$file_data[$line_number]=$user_data;
			$line_number=$line_number+1;

		}
		
		return $file_data;
		
	}*/
	
	
	/*
		Function that creates the CSV File
		return: CSV File
	*/
	function createCSV(){
	
		$file_data=data_matrix();
		
		if(count($file_data) == 0)
			echo "There no Data on Database !";

				
		$fp=fopen('php://memory','w');
		
		foreach($file_data as $values){
			fputcsv($fp,$values);
		}
		fseek($fp,0);
		header('Content-Type: application/csv');
		header('Content-Disposition: attachment; filename="data.csv";');
		fpassthru($fp);
	}
	
	createCSV();
	
?>