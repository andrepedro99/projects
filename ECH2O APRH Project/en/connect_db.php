<?php
	function connectDB(){
		$servername="localhost";
		$user="root";
		$pwd="";
		$dbname="ech2o";

		$conn=new mysqli($servername,$user,$pwd,$dbname);
		if ($conn->connect_error){ 
			die("Connection Failed: " . $conn->connect_error);
		}
		
		$conn->set_charset("utf8");
		
		return $conn;
	}
?>