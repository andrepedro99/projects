

<?php include('../menu.html');?>


<div class="container-fluid">
	
	
	<div class="row" style="background-color:#afdded;">
		&nbsp;
	</div>
	
	<div class="row"  id="stend_page" style="">
				
				<!-- Index Image Area -->
				<div class="col-sm-8" style="margin-top:6%; margin-bottom:10%;">
					<div style="margin-left:12%;">
						<a href="questions.php" style="text-decoration:none;">
							<div class="index_img"><img src="../images/index_img_en.png" class="img-fluid"></div>
						</a>
					</div>
				</div>
				
				<!-- Index Text Area -->
				<div class="col-sm-4" style="background:rgba(255,255,255,0.3);">
					<div class="index_text_area sticky-top">
						<div style="font-size:25px; font-weight: bold;">What is the Water Footprint ?</div><br>
						A Pegada Hídrica (PH) é um indicador do uso de água doce por unidade de tempo, por pessoa, produto ou serviço, 
						incluindo todos os consumos associados direta e indiretamente
						<br>(Haida <i>et al.</i>, 2019). 
					</div>
					<br>
				</div>
	</div>
	
	<div class="row" style="background-color:#afdded;">
		&nbsp;
	</div>
	
</div>


<?php include('../footer.html'); ?>

