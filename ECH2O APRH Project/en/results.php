<?php

	include 'connect_db.php';

	
	/*
		Function that returns ids from questions
		return: List of questions identifications 
	*/
	function getQuestionIds(){
		
		$conn=connectDB();
		
		$sql="SELECT id FROM perguntas_en ORDER BY id ASC";
		$result=$conn->query($sql);
		
		$id_questions=array();
		$count=0;
		if($result->num_rows>0){
			while($row=$result->fetch_assoc()){
				$id_questions[$count]=$row["id"];
				$count=$count+1;
			}
		}
		
		return $id_questions;
	}
	
	
	/*
		Function that inserts a new user in the Database
		args: Date and User Unique identifier 
	*/
	function createNewUser($date,$hash_id){
		$conn=connectDB();
		$sql="INSERT INTO utilizadores (time,hash_id) VALUES ('$date','$hash_id');";
		$result=$conn->query($sql);
		
		if($result==True){
			$cookie_name="usr_hash";
			$cookie_value=$hash_id;
			$cookie_timer=time()+6200;
			setcookie($cookie_name,$cookie_value,$cookie_timer,'/');
		}
	}
	
	
	/*
		Function that returns the water values references
		return: List of Questions Refererences
	*/
	function getWater_Ref_Values(){
		$conn=connectDB();
	
		$water_values=array();
		
		$sql="SELECT perguntas_en.id AS id_per, referencias.valor_agua FROM perguntas_en INNER JOIN referencias WHERE 
			perguntas_en.id_ref=referencias.id ORDER BY perguntas_en.id ASC;";
		$result=$conn->query($sql);
		while($row=$result->fetch_assoc())
		{
			$id_question=$row["id_per"];
			$water_ref=$row["valor_agua"];
			$water_values[$id_question]=$water_ref;
		}
		
		return $water_values;
	}
	
	
	/*
		Function that uploads user answers
		args: List of User Answers and User Unique Identifier
		return: List of Questions Refererences
	*/
	function uploadAnswers($user_answers,$hash_id){
		
		$conn=connectDB();
		
		$sql_userId="SELECT id FROM utilizadores WHERE hash_id='$hash_id';";
		$result_userId=$conn->query($sql_userId);
		
		$userId="";
		if($result_userId->num_rows>0){
			while($row_userId=$result_userId->fetch_assoc()){
				$userId=$row_userId["id"];
			}
		}
		
		
		foreach($user_answers as $id_per=>$value){
			$sql="INSERT INTO dados (id_user,id_per,resultado) VALUES ('$userId','$id_per','$value');";
			$result=$conn->query($sql);
		}
		
	}
	
	
	/*
		Function that uploads the final result
		args: Results and User Unique Identifier
		return: List of Questions Refererences
	*/
	function uploadFinalResult($result_01,$result_02,$hash_id){
		
		$conn=connectDB();
		
		$sql_userId="SELECT id FROM utilizadores WHERE hash_id='$hash_id';";
		$result_userId=$conn->query($sql_userId);
		
		$userId="";
		if($result_userId->num_rows>0){
			while($row_userId=$result_userId->fetch_assoc()){
				$userId=$row_userId["id"];
			}
		}
		
		$sql="INSERT INTO dadosfinais (id_user,resultado_rea,resultado_vir) VALUES ('$userId','$result_01','$result_02');";
		$result=$conn->query($sql);
		/*if($result==True)
			echo "Inserted Successfully";
		else
			echo "Insert went Wrong";
		*/
		
	}
	
	
	/*
		Function that returns the temporal references
		return: List of Temporal References
	*/
	function getTime_Ref(){
		
		$conn=connectDB();
		
		$temp_val=array();
		
		$sql="SELECT perguntas_en.id AS id_per, perguntas_en.id_temp, tempo.id, tempo.valor FROM perguntas_en INNER JOIN tempo 
		WHERE perguntas_en.id_temp=tempo.id ORDER BY perguntas_en.id ASC;";
		$result=$conn->query($sql);
		while($row=$result->fetch_assoc()){
			$id_question=$row["id_per"];
			$temp_ref=$row["valor"];
			$temp_val[$id_question]=$temp_ref;
		}
		
		return $temp_val;
	}
	
	
	/*
		Function that returns the temporal references
		return: List of Temporal References
	*/
	function calWaterResult(){
	
		$date=date("Y-m-d H:i:s.u");
		$hash_id=hash('md5',$date);
		
		createNewUser($date,$hash_id);
		
		$id_questions=getQuestionIds();
		$id_questionsLen=count($id_questions);
		
		
		$user_answers=array();
		
		$water_ref=getWater_Ref_Values();
		$time_ref=getTime_Ref();
		
		for($i=0;$i<$id_questionsLen;$i++)
		{
			$id_question=$id_questions[$i];
			
			if($id_question==3 || $id_question==4){
				if(!empty($_POST[$id_question]))
					$ans=strip_tags($_POST[$id_question]);
				else
					$ans=0;
			}
			else if(empty($_POST[$id_question]) || !is_numeric($_POST[$id_question]) || $_POST[$id_question]<0)
				$ans=0;
			else
				$ans=$_POST[$id_question];
			
			$user_answers[$id_question]=$ans;	
		}
		
		//print_r($user_answers);
		
		//print_r($user_answers);
		//echo $user_answers[13]."<br>";
		
		if($user_answers[17]==0)
			$user_answers[17]=1;
		
		//echo $user_answers[13]."<br>";
		
		$f_01=($user_answers[7]*$user_answers[6])+($user_answers[8]*$user_answers[9]*15)+
		($user_answers[11])+($user_answers[12]*3)+($user_answers[13]*$user_answers[14])+
		((($user_answers[16]*50)/$user_answers[17])/7)+((($user_answers[19]*22)/$user_answers[17])/7)+
		(($user_answers[21]*$user_answers[22]*12)/$user_answers[17]);
		
		/*$f_01=$user_answers[4]+($user_answers[5]*$user_answers[6]*15)+$user_answers[7]+($user_answers[9]*3)+
		($user_answers[10]*$user_answers[11])+((($user_answers[12]*50)/$user_answers[13])/7)+((($user_answers[15]*22)/$user_answers[13])/7)+
		(($user_answers[17]*$user_answers[18]*12)/$user_answers[13]);
		*/
		$f_02=0;
		
		for($i=23;$i<=46;$i++){
			$tmp=($user_answers[$i]*$water_ref[$i])/$time_ref[$i];
			$f_02=$f_02+$tmp;
		}
		
		$GLOBALS["result01"]=round($f_01);
		$GLOBALS["result02"]=round($f_02);
		
		//uploadAnswers($user_answers,$hash_id);
		//uploadFinalResult($f_01,$f_02,$hash_id);
		
	}
	
	
	//if($_SERVER["REQUEST_METHOD"]=="POST")
			//calWaterResult();
	/*else{
		http_response_code(404);
		die();
	}*/
?>



<?php include('../menu.html'); ?>


<div class="container-fluid">
	
	<div class="row" style="background-color:#afdded;">
		&nbsp;
	</div>
	
	<div class="row" id="stend_page" style="">
				
				<div class="col-sm-8 d-none d-sm-block" style="background-color:#4788B6;">
					<video width="100%" height="100%" autoplay muted loop class="embed-responsive-item">
						<source src="../videos/result_video.mov" type="video/mp4">
					</video>
				</div>
				
				
				<div class="col-sm-12 d-block d-sm-none" style="background-color:#4788B6; padding-bottom:50%;">
					<div style="margin-top:50%;">
					<video width="100%" height="140%" autoplay muted loop class="embed-responsive-item">
						<source src="../videos/result_video.mov" type="video/mp4">
					</video>
					</div>
				</div>
				
			
				<div class="col-sm-4 d-none d-sm-block" style="background:rgba(255,255,255,0.4);">
					<div class="results_text_area">
						A sua pegada hídrica atual é de: <br>
						
						&#9642; Direct Consumption: <div class="results_text"><?php echo $GLOBALS['result01']; echo "&nbsp; L/dia"; ?></div>
						&#9642; Indirect Consumption: <div class="results_text"><?php echo $GLOBALS['result02']; echo "&nbsp; L/dia"; ?></div>
						
						
						<?php
							if($GLOBALS['result01']<=150)
								echo "Está no bom caminho, mas ainda pode diminuir a sua Pegada Hídrica ...
								Não há Planeta B!";
							else
								echo "Finalizou o questionário com sucesso, a sua Pegada Hídrica é elevada! Deve mudar os
									seus hábitos para a reduzir... 
									Não há Planeta B!";
							
						?>
					</div>
				</div>
				
				
					
					<div id="mod_result" class="modal fade" role="dialog">
						<div class="modal-sm modal-dialog">
						<!-- Modal content-->
							<div class="modal-content" style="background-color:#29728c;">
								<div class="d-flex flex-row-reverse">
										<button type="button" class="close" data-dismiss="modal" style="color:white; margin-top:5%; margin-right:5%;">&times;</button>
								</div>
								<div class="modal-body" style="background-color:#29728c;">
									<div class="results_text_area">
										A sua pegada hídrica atual é de: <br>
						
										&#9642; Direct Consumption: <div class="results_text"><?php echo $GLOBALS['result01']; echo "&nbsp; L/dia"; ?></div>
										&#9642; Indirect Consumption: <div class="results_text"><?php echo $GLOBALS['result02']; echo "&nbsp; L/dia"; ?></div>
						
						
										<?php
											if($GLOBALS['result01']<=150)
												echo "Está no bom caminho, mas ainda pode diminuir a sua Pegada Hídrica...
												Não há Planeta B!";
											else
												echo "Finalizou o questionário com sucesso, a sua Pegada Hídrica é elevada! Deve mudar os
												seus hábitos para a reduzir... 
												Não há Planeta B!";
							
										?>
									</div>
								</div>
								<br>
							</div>

						</div>
					</div>
 				
	</div>

	<div class="row" style="background-color:#afdded;">
		&nbsp;
	</div>
</div>


<script>
	setTimeout(function(){
		if(window.innerWidth<600){
			$("#mod_result").modal('show');
		}
	},5000);
</script>


<div>
	<?php include('../footer.html'); ?>
</div>

