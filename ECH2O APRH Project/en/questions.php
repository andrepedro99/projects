﻿<?php

	include 'connect_db.php';
	
	
	/*
		Function that returns subcategories from database
		return: List of subcategories 
	*/
	function mainData(){
		$conn=connectDB();
		
		$sql="SELECT categorias_en.nome_cat,subcategorias_en.id AS subcat_id,subcategorias_en.nome_subcat, perguntas_en.id AS id_per
		FROM perguntas_en 
		INNER JOIN subcategorias_en,categorias_en WHERE perguntas_en.id_subcat=subcategorias_en.id AND subcategorias_en.id_cat=categorias_en.id AND perguntas_en.id 
		NOT IN (SELECT id_per_rel FROM relacoes_en) GROUP BY subcat_id ORDER BY id_per ASC;";
		$result=$conn->query($sql);
	
		return $result;
	}
	
	
	
	/*
		Function that checks of questions with multiple answers
		args: Question ID
	*/
	function checkMulAns($id_per){
		$conn=connectDB();
		
		$sql="SELECT * FROM pergunta_resposta_en WHERE id_per=$id_per";
		$result=$conn->query($sql);
		
		if($result->num_rows>0)
			return true;
		else
			return false;
	}
		
	
	/*
		Function that returns answers from a specific question
		args: Question ID
		return: List of Answers 
	*/
	function getAnswers($id_per){
	
		$conn=connectDB();
		
		$sql01="SELECT id_resposta FROM pergunta_resposta_en WHERE id_per=$id_per";
		$result01=$conn->query($sql01);
		
		$id_answers=array();
		$count=0;
		if($result01->num_rows>0){
			while($row01=$result01->fetch_assoc()){
				$id_answers[$count]=$row01["id_resposta"];
				$count=$count+1;
			}
		}
		
		$n=count($id_answers);	
		$answers=array();
		$count_a=0;
		for($i=0;$i<$n;$i++){
			$id_resposta=$id_answers[$i];
			$sql02="SELECT nome_resposta FROM respostas_en WHERE id=$id_resposta;";
			$result02=$conn->query($sql02);
			if($result02->num_rows>0)
			{
				while($row02=$result02->fetch_assoc()){
					$answers[$count_a]=$row02["nome_resposta"];
					$count_a=$count_a+1;
				}
			}
		}
		
		return $answers;
	
	}
	
	
	/*
		Function that returns answers values
		args: List of Answers
		return: List of answers values 
	*/
	function getAnswerValue($answers){
		
		$conn=connectDB();
		
		$ans_values=array();
		$count=0;
		foreach($answers as $i)
		{
			$sql="SELECT valor FROM respostas_en WHERE nome_resposta='$i';";
			$result=$conn->query($sql);
			if($result->num_rows>0)
			{
				while($row=$result->fetch_assoc()){
					if($row["valor"]==-1)
						$ans_values[$count]=$i;
					else
						$ans_values[$count]=$row["valor"];
					$count=$count+1;
				}
			}
		}
		
		return $ans_values;
	}
	
	
	/*
		Function that returns related questions
		args: Question ID
		return: List of related questions 
	*/
	function getRelatedQuestion($id_per){
		
		$conn=connectDB();
		
		$sql01="SELECT id_per_rel FROM relacoes_en WHERE id_per_ac=$id_per;";
		$result01=$conn->query($sql01);
		
		$rel_ids=array();
		$count=0;
		if($result01->num_rows>0){
			while($row=$result01->fetch_assoc()){
				$id_rel=$row["id_per_rel"];
				$rel_ids[$count]=$id_rel;
				$count=$count+1;
			}
		}
		
		
		$result_id=array();
		$result_question=array();
		$count=0;
		
		foreach($rel_ids as $i)
		{
			$sql="SELECT categorias_en.nome_cat,subcategorias_en.nome_subcat,perguntas_en.id AS id_per,perguntas_en.num_per,perguntas_en.pergunta FROM perguntas_en 
			INNER JOIN subcategorias_en,categorias_en WHERE perguntas_en.id_subcat=subcategorias_en.id AND subcategorias_en.id_cat=categorias_en.id AND perguntas_en.id=$i;";
			$result=$conn->query($sql);
		
			if($result->num_rows>0)
			{
				while($row=$result->fetch_assoc()){
					$result_id[$count]=$row["id_per"];
					$result_question[$count]=$row["pergunta"];
					$count=$count+1;
				}
			}
		}
		
		
		$rel_question=array();
		$len_arr=count($result_id);
		
		for($i=0;$i<$len_arr;$i++)
			$rel_question[$result_id[$i]]=$result_question[$i];
		
		return $rel_question;
	}
	
	
	
	/*
		Function that returns images names
		return: List of images names
	*/
	function getImages(){
		
		$conn=connectDB();
		$sql="SELECT subcategorias_en.imagem FROM perguntas_en 
		INNER JOIN subcategorias_en,categorias_en WHERE perguntas_en.id_subcat=subcategorias_en.id 
		AND subcategorias_en.id_cat=categorias_en.id 
		GROUP BY subcategorias_en.id ORDER BY perguntas_en.id ASC;";
		/*$sql="SELECT categorias.nome_cat,subcategorias.id AS subcat_id,subcategorias.nome_subcat, perguntas.id AS id_per, subcategorias.fundo
		FROM perguntas 
		INNER JOIN subcategorias,categorias WHERE perguntas.id_subcat=subcategorias.id AND subcategorias.id_cat=categorias.id AND perguntas.id 
		NOT IN (SELECT id_per_rel FROM relacoes) GROUP BY subcat_id ORDER BY id_per ASC;";*/
		$result=$conn->query($sql);
		$imgs=array();
		$count=0;
		if($result->num_rows>0)
		{
			while($row=$result->fetch_assoc()){
				if(!empty($row["imagem"]))
					$imgs[$count]=$row["imagem"];
				$count=$count+1;
			}
		}
		
		return $imgs;
	}


	/*
		Function that returns descriptions
		return: List of descriptions
	*/
	function getDescription(){
		
		$conn=connectDB();
		$sql="SELECT subcategorias.descricao FROM perguntas 
		INNER JOIN subcategorias,categorias WHERE perguntas.id_subcat=subcategorias.id 
		AND subcategorias.id_cat=categorias.id 
		GROUP BY subcategorias.id ORDER BY perguntas.id ASC;";
		$result=$conn->query($sql);
		$descriptions=array();
		$count=0;
		if($result->num_rows>0)
		{
			while($row=$result->fetch_assoc()){
				if(!empty($row["descricao"]))
					$descriptions[$count]=$row["descricao"];
				$count=$count+1;
			}
		}
		return $descriptions;
	}
	
	/*
		Function that returns questions of a certain subcategory
		args: Subcategory ID
		return: List of Questions
	*/
	function get_subcat($id_subcat){
		
		$conn=connectDB();
		$sql="SELECT id,pergunta FROM perguntas_en WHERE id_subcat=$id_subcat AND id 
		NOT IN (SELECT id_per_rel FROM relacoes_en)";
		$result=$conn->query($sql);
		
		$tmp=array();
		
		if($result->num_rows>0)
		{
			while($row=$result->fetch_assoc()){
				$id_per=$row["id"];
				$pergunta=$row["pergunta"];
				$tmp[$id_per]=$pergunta;
			}
		}
		
		return $tmp;
	}
	
	
	/*
		Function that returns number of question on
		the subcategory
		args: Question ID
		return: Number of the Question
	*/
	function get_num_per($id_per){
		$conn=connectDB();
		$sql="SELECT num_per FROM perguntas_en WHERE id=$id_per";
		$result=$conn->query($sql);
		
		if($result->num_rows>0)
		{
			while($row=$result->fetch_assoc()){
				$num_per=$row["num_per"];
			}
		}
		
		return $num_per;
	}
	
	
	/*
		Function that returns descriptions
		return: List of descriptions
	*/
	function getBackground(){
		
		$conn=connectDB();
		$sql="SELECT subcategorias_en.fundo FROM perguntas_en 
		INNER JOIN subcategorias_en,categorias_en WHERE perguntas_en.id_subcat=subcategorias_en.id 
		AND subcategorias_en.id_cat=categorias_en.id 
		GROUP BY subcategorias_en.id ORDER BY perguntas_en.id ASC;";
		$result=$conn->query($sql);
		$descriptions=array();
		$count=0;
		if($result->num_rows>0)
		{
			while($row=$result->fetch_assoc()){
				if(!empty($row["fundo"]))
					$descriptions[$count]=$row["fundo"];
				$count=$count+1;
			}
		}
		return $descriptions;
	}
	
	
?>


<?php include('../menu.html');?>  
  
  <script>
			//Convert from PHP Array into JavaScript Array
			
			function getArray(){
				var imgs=[];
				<?php
					$imgs=getImages();
					foreach($imgs as $i)
					{	
						?>
						//Images Names
						imgs.push("<?php echo $i;?>");
						<?php
					}
				?>
				return imgs;
			}
			
			function getDes(){
				var desc=[];
				<?php
					$desc=getDescription();
					foreach($desc as $j)
					{
						?>
						//Descriptions
						desc.push("<?php echo $j;?>");
						<?php
					}
				?>
			
				return desc;
			}		
			
			function getBg(){
				var backs=[];
				<?php
					$backs=getBackground();
					foreach($backs as $j)
					{
						?>
						//Backgrounds
						backs.push("<?php echo $j;?>");
						<?php
					}
				?>
			
				return backs;
			}
			
   </script>
  

<div class="container-fluid">
	<div class="row" style="background-color:#afdded;">
		&nbsp;
	</div>
	
	<div class="row" id="main_image" style="min-height:auto;">
		<div class="col-sm-12">
			<div class="row" style="">
				
				<div class="col-sm-8" style="background:rgba(255,255,255,0.1);">
					
					<div style="position:absolute; left:8%; margin-top:2%;">
						<img id="image_question" src="../images/" width="60%" height="50%" class="img-fluid">
					</div>
				
					
					<form id="regForm" method="POST" action="results.php">
					<?php
						$sub_cats=array();
						$count=0;
						$resultDB=mainData();
						if($resultDB->num_rows>0)
						{
							while($row=$resultDB->fetch_assoc())
							{	
								$subcat_id=$row["subcat_id"];
								$sub_cats[$count]=$subcat_id;
								$count = $count + 1;
								
								$GLOBALS["sub_cats"]=$sub_cats;
								$subcats=get_subcat($subcat_id);
								
								echo "<div class=".'"tab"'.">";
									$nome_cat=$row["nome_cat"];
									$nome_subcat=$row["nome_subcat"];								
									//$GLOBALS["background"]=$row["fundo"];
									//echo $row["fundo"];
									
									/*echo "<div>";
											//Category Name
											echo "<h1>$nome_cat</h1>";
									echo "</div>";
									
									echo "<div style=".'"color: white; padding-right:10%; padding-top:3%; padding-bottom:5%;"'.">";
										//Subcategory Name
										echo "<h3>$nome_subcat</h3>";
									echo "</div>";*/
									
									foreach($subcats as $id_per=>$pergunta)
									{	
										$num_per=get_num_per($id_per);
									echo "<div class=".'"cat"'.">";
									
										echo "<div>";
											//Question
											echo "$num_per. $pergunta";
										echo "</div>";
										
										//Answers
										echo "<div style=".'""'.">";
											if(checkMulAns($id_per))
											{
												$answers=getAnswers($id_per);
												$values=getAnswerValue($answers);
												$num_answers=count($answers);
											
												for($i=0;$i<$num_answers;$i++)
												{
													//Answers with Radio Buttons
													if($answers[$i]=="Yes" || $answers[$i]=="Têm a opção de dupla descarga")
													{
														echo "<table>";
															echo "<tr>";
																echo "<td><input style=".'"width:0.8em; height:0.8em; "'."  type=".'"radio"'." name=$id_per  value=$values[$i] onclick='showQuestion($id_per);'/></td>";
																echo "<td>&nbsp; $answers[$i]</td>";
															echo "</tr>";
														echo "</table>";
														
														//Related Questions
														$rel_question=getRelatedQuestion($id_per);
														echo "<div id='$id_per' class='hide' style=".'"margin-top:2%; margin-left:3%;"'.">";
														foreach($rel_question as $id_rel=>$pergunta)
														{
															$num_per_rel=get_num_per($id_rel);
															echo $num_per_rel.". ".$pergunta."<br>"; //Related Question Text
															echo "<input type=".'"number"'."min=0 name=$id_rel  style=".'"width:8em; border-radius:15px; color:#29728c; font-family:Roboto; font-weight:bold; font-size:20px;"'."><br>";
															echo "<br>";
														}
														echo "</div>";
													}
													else if($answers[$i]=="No" || $answers[$i]=="Não têm a opção de dupla descarga"){
														echo "<table>";
															echo "<tr>";
																echo "<td><input style=".'"width:0.8em; height:0.8em; "'."  type=".'"radio"'." name=$id_per  value=$values[$i] onclick='hideQuestion($id_per);'/></td>";
																echo "<td>&nbsp; $answers[$i]</td>";
															echo "</tr>";
														echo "</table>";
													
													}
													//Answers without Related Question Display
													else{
														echo "<table>";
															echo "<tr>";
																echo "<td><input style=".'"width:0.8em; height:0.8em; "'."  type=".'"radio"'." name=$id_per  value=$values[$i]></input></td>";
																echo "<td>&nbsp; $answers[$i]</td>";
															echo "</tr>";
														echo "</table>";
													}
												}
											}
											else
												//Another type of Answers
												if($id_per==3 || $id_per==4)
													echo "<input class=".'""'."type=".'"text"'." id=".'"ans_text"'."name=$id_per style=".'"width:8em; height:2.2em; border-radius:15px; color:#29728c; font-family:Roboto; font-weight:bold; font-size:20px;"'.">";
												else
													echo "<input class=".'""'."type=".'"number"'." id=".'"ans_text"'."min=0 name=$id_per style=".'"width:8em; height:2.2em; border-radius:15px; color:#29728c; font-family:Roboto; font-weight:bold; font-size:20px;"'.">";
										echo "</div>";
									echo "</div>";
									}	
								echo "</div>";
							}
						}
					?>
					
					
					<div class="d-flex justify-content-center d-block d-sm-none" style="margin-top:5%; margin-bottom:2%;">
						<button type="button" class=" plus_button d-block d-sm-none" data-toggle="modal" data-target="#mod">
								<img src="../images/n.png" class="img-fluid"> <div style="color:#29728c; font-size: 15px; font-weight:bold;">Mais Info</div>
						</button>
					</div>
					
					
					<div style="overflow:auto;">
						<div class="button_pos">
							<button type="button" class="b_form" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
							&nbsp;
							<button type="button" class="b_form"id="nextBtn" onclick="nextPrev(1)">Next</button>
						</div>
					</div>
					
					
					
					
					<div id="mod" class="modal fade" role="dialog">
						<div class="modal-sm modal-dialog">
						<!-- Modal content-->
							<div class="modal-content" style="background-color:#29728c;">
								<div class="modal-body" style="background-color:#29728c;">
									<div class="d-flex flex-row-reverse">
										<button type="button" class="close" data-dismiss="modal" style="color:white;">&times;</button>
									</div>
									<div id="question_description_m" style="font-family:Roboto; font-size:14px; color:white; margin-left:5%; margin-right:5%;">
							
									</div>
								</div>
								<br>
								
							</div>

						</div>
					</div>
					
					
					
					
					<div style="text-align:center; padding-top:4%;">
						<!-- Steps Section -->
						<?php
							$resultDB=mainData();
							for($i=0;$i<$resultDB->num_rows;$i++){
								echo "<span class=".'"step"'."></span>";
							}
						?>
					</div>
					</form>
				</div>
				<div class="col-sm-4 d-none d-sm-block" style="background-color:#29728c;">
					<div class="sticky-top" style="margin-top:15%; top:5%;">
						<div class="" id="question_description" style="font-family:Roboto; color:white; font-size:20px; margin-left:5%; margin-right:5%;"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row" style="background-color:#afdded;">
		&nbsp;
	</div>
</div>




<script>
var currentTab = 0; // First tab equals 0
showTab(currentTab); // Current Tab

function showTab(n) {

  document.body.scrollTop=0; //Forces scroll to top of page
  document.documentElement.scrollTop=0;
  
  //console.log(currentTab);
  document.getElementById('question_description').innerHTML=""; //Get actual description
  
  var imgs=getArray();  //Image Names
  var img_name=imgs[currentTab];
  
  console.log(img_name);
  document.getElementById('image_question').src="../images/"+img_name;
  
  var desc=getDes(); //Descriptions
  var des=desc[currentTab];
  console.log(des);
  
  document.getElementById('question_description_m').innerHTML="";
  if(des != null){
	document.getElementById('question_description').innerHTML+=des;
	document.getElementById('question_description_m').innerHTML+=des;
  }
  else{
	document.getElementById('question_description').innerHTML="";
	document.getElementById('question_description_m').innerHTML="";
  }
  
  var bgs=getBg();
  var bg=bgs[currentTab];
  console.log(bg);
  document.getElementById('main_image').style.backgroundImage="url('../images/"+bg+"')";
  
  
  
  var x = document.getElementsByClassName("tab"); //Actual Tab
  x[n].style.display = "block";
  
  //Buttons of the Form
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none"; 
  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "Calculate";
  } else {
    document.getElementById("nextBtn").innerHTML = "Next";
  }
  
  fixStepIndicator(n) //Verifies the current step
}

function nextPrev(n) {
  
  var x = document.getElementsByClassName("tab"); //Actual Tab
  if (n == 1 && !validateForm()) return false;
  x[currentTab].style.display = "none";
  currentTab = currentTab + n; //Calculates the next step
  if (currentTab >= x.length) {
    document.getElementById("regForm").submit();
    return false;
  }
  showTab(currentTab); // Show the correct tab
 
}

function validateForm() {
  //Function that shows the last step
  var x, y, i, valid = true;
  if (valid) {
	document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; 
}

function fixStepIndicator(n) {
  //Function that changes the steps from inactive to active
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }

  x[n].className += " active";
}


//Hide Related Questions
function hideQuestion(id){
	document.getElementById(id).style.display='none';
}

//Show Related Questions
function showQuestion(id){
	document.getElementById(id).style.display='block';
}
</script>


<?php include('../footer.html'); ?>


