import feedparser
import pymongo
import threading
from multiprocessing.dummy import Pool as ThreadPool
import datetime


def connect_db():
    """Function used to connection the MongoDB DataBase"""
    try:
        client = pymongo.MongoClient("mongodb://127.0.0.1:27017/")
        db = client["feedsDB"]
    except pymongo.errors.ConnectionFailure as e:
        print("Warning: There has been a Error on DB Connection")
        print("Error Status: "+e)
    return db


database = connect_db()


class FeedAlert:
    """
        This Class implements the methods, responsible to execute
        users inserted commands, and it must interact with the database
    """
    def send_active_feeds(self):
        """
            :return: Updates the Active Feeds List
            Warning: Document must exist on collection programData
        """
        col = database["programData"]
        condition = {
            "title": "Active Feeds"
        }

        update_data = {
            "$set": {
                "data": active_feeds
            }

        }
        col.update_one(condition, update_data)

    def get_active_feeds(self):
        """
            :return: Receives active feeds from database
            Warning: Document must exist on collection programData
        """
        tmp = []
        col = database["programData"]
        query = {
            "title": "Active Feeds",
            "data": {"$exists": True, "$ne": []}
        }
        results = col.find(query)
        for i in results:
            tmp = i['data']

        return tmp

    def send_active_alerts(self):
        """
            :return: Updates the Active Alerts List
            Warning: Document must exist on collection programData
        """
        col = database["programData"]
        condition = {
            "title": "Active Alerts",
        }
        update_data = {
            "$set":{
                "data": alerts
            }
        }
        col.update_one(condition, update_data)

    def get_active_alerts(self):
        """
            :return: Receives active feeds from database
            Warning: Document must exist on collection programData
        """
        tmp = []
        col = database["programData"]
        query = {
            "title": "Active Alerts"
        }
        results = col.find(query)
        for i in results:
            tmp = i['data']

        return tmp

    def check_repeated(self):
        """
            :return: Receives all id from collection Reports
        """
        col = database["reports"]
        results = col.find({}, {"_id": 0, "guid": 1})
        for i in results:
            ids.append(i['guid'])

    def add_feed(self, url):
        """
            :param url: Inserted from User
            :return: Adds a new feed on collection Reports
        """
        self.check_repeated()
        feed = feedparser.parse(url)
        if url not in active_feeds:
            active_feeds.append(url)
            self.send_active_feeds()

        ins_data = []

        for i in feed.entries:
            date = datetime.datetime.strptime(str(i['published_parsed'][0])+"-"+str(i['published_parsed'][1])+"-" +
                                              str(i['published_parsed'][2])+"T"+str(i['published_parsed'][3])+":" +
                                              str(i['published_parsed'][4])+":"+str(i['published_parsed'][5]),
                                              "%Y-%m-%dT%H:%M:%S")
            d = {
                "title": i['title'],
                "summary": i['summary'],
                "published": date,
                "guid": i['guid'],
                "origin": url
            }

            if i['guid'] not in ids:
                ins_data.append(d)

        col = database["reports"]
        if len(ins_data) != 0:
            col.insert_many(ins_data)
            print("Feed Source has been Added")
        elif len(feed.entries) == 0:
            print("No Feeds for given URL")
        else:
            print("Warning: All feeds are already inserted on the Database.")

        self.send_active_feeds()

    def remove_feed(self, url):
        """
            :param url: Inserted from user
            :return: Removes a source feed on the collection Reports
        """
        if url in active_feeds:
            col = database["reports"]
            query = {
                "origin": url
            }
            col.delete_many(query)
            active_feeds.remove(url)
            ids.clear()
            self.check_repeated()
            self.send_active_feeds()
            print("Source Feed has been Removed Successfully")
        else:
            print("Warning: There is no Feed with inserted Requirements")

    def load_file(self, filename):
        """
            :param filename: Inserted from user
            :return: Loads file with feeds and adds into collection Reports using function add_feed
        """
        file = open(filename, "r")
        tmp = file.read().split()
        for i in tmp:
            if i not in active_feeds:
                self.add_feed(i)
            else:
                continue

    def title_contains(self, search_param):
        """
            :param search_param: Inserted by User
            :return: Search for news with a specific word on title
        """
        col = database["reports"]
        col.create_index([("title", pymongo.TEXT)], name='search_title')
        query = {
            "$text": {
                "$search": search_param
            }
        }
        results = col.find(query)
        print("\n")
        for i in results:
            print("Title: "+str(i['title']))
            print("Summary: "+str(i['summary']))
            print("Published: "+str(i['published']))
            print("GUID: "+str(i['guid']))
            print("\n")

    def alert_on(self, search_param):
        """
            :param search_param: Inserted from user
            :return: Creates a new alert who`s title an description matches search parameter
        """
        if search_param not in alerts:
            alerts.append(search_param)
            self.send_active_alerts()

        col = database["reports"]
        col.create_index([("title", pymongo.TEXT)], name='search_title')
        query = {
            "$text": {"$search": search_param},
            "summary": {"$regex": str(search_param)}
        }
        results = col.count_documents(query)
        if results > 0:
            print("Alert: There is "+str(results)+" new feeds about "+str(search_param))

    def remove_alert(self, search_param):
        """
            :param search_param: Inserted from user
            :return: Removes a alert notification with a specific word
        """
        if search_param in alerts:
            alerts.remove(search_param)
            self.send_active_alerts()
            print("Alert has been Removed")
        else:
            print("Warning: There is no Alert with inserted Requirements")

    def update_data(self):
        """
            :return: Creates thread pool for active feeds.
            Note: Details for thread pool inside the Final Report.
        """
        num_threads = len(active_feeds)
        pool = ThreadPool(num_threads)
        pool.map(self.add_feed, active_feeds)
        pool.close()
        pool.join()

    def set_update_time(self):
        """
            :return: Sets update time for active feeds
            Note: Program Parameter is defined in this function
        """
        time = 10.0
        threading.Timer(time, self.update_data).start()
        self.update_data()

    def check_for_alerts(self):
        """
            :return: Creates thread pool for active alerts.
            Note: Details for thread pool inside the Final Report.
        """
        num_threads = len(alerts)
        pool_alert = ThreadPool(num_threads)
        pool_alert.map(self.alert_on, alerts)
        pool_alert.close()
        pool_alert.join()

    def set_alert_time(self):
        """
            :return: Sets update time for active alerts
            Note: Program Parameter is defined in this function
        """
        time = 10.0
        threading.Timer(time, self.check_for_alerts).start()
        self.check_for_alerts()


f = FeedAlert()
active_feeds = f.get_active_feeds()
alerts = f.get_active_alerts()
ids = []


def main():
    """
        This is the main function.
        Enumerates user options and call class functions.
    """
    options = [1, 2, 3, 4, 5, 6, 7]
    name_options = ["add-feed <feed> : Add a New Feed", "remove-feed <feed> : Remove a Feed",
                    "load <filename> : Loads Feeds from File",
                    "title-contains <search_param> : Search for String on Title",
                    "alert-on <alert_param> : Alert user when a News Appears",
                    "alert-off <alert param> : Remove Alert Notification", "exit: Exits the Program"]

    print("---- Feed Scraper ----\n")
    print("Options:")
    for i, j in zip(options, name_options):
        print(str(i) + " - " + str(j))
    print("\n")

    if active_feeds:
        print("\n")
        f.set_update_time()
        print("\n")

    if alerts:
        print("\n")
        f.set_alert_time()
        print("\n")

    while True:
        try:
            option = input("Insert Option: ")
            args = option.split()
            if args[0] == "add-feed":
                if len(args) >= 2:
                    url = args[1]
                    f.add_feed(url)
                else:
                    print("Invalid Syntax")
            elif args[0] == "remove-feed":
                if len(args) >= 2:
                    url = args[1]
                    f.remove_feed(url)
                else:
                    print("Invalid Syntax")
            elif args[0] == "load":
                if len(args) >= 2:
                    filename = args[1]
                    f.load_file(filename)
                else:
                    print("Invalid Syntax")
            elif args[0] == "title-contains":
                if len(args) >= 2:
                    search_param = args[1]
                    f.title_contains(search_param)
                else:
                    print("Invalid Syntax")
            elif args[0] == "alert-on":
                if len(args) >= 2:
                    search_param = args[1]
                    f.alert_on(search_param)
            elif args[0] == "alert-off":
                if len(args) >= 2:
                    search_param = args[1]
                    f.remove_alert(search_param)
                else:
                    print("Invalid Syntax")
            elif args[0] == "exit":
                f.send_active_feeds()
                f.send_active_alerts()
                print("Bye")
                break
            else:
                print("Invalid Option")
        except:
            print("Warning: There has been an unexpected error")

    return 0


if __name__ == "__main__":
    main()
